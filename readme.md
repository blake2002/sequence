
1. 在工程中引用sequence项目的sequence-client jar包
2. 在的应用数据库中创建两个sequence表，sql语句如下：
```
    CREATE TABLE `sequence0` (
        `value` bigint(20) NOT NULL,
        `name` varchar(50) NOT NULL,
        `gmt_create` datetime NOT NULL,
        PRIMARY KEY (`name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE `sequence1` (
        `value` bigint(20) NOT NULL,
        `name` varchar(50) NOT NULL,
        `gmt_create` datetime NOT NULL,
        PRIMARY KEY (`name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

3. 在spring xml中配置如下，为了防止单点故障，你可以配置两个数据库源:
```
    <bean id="bizOrderIdSequence" class="com.oschina.sequence"
                   init-method="init" destroy-method="destroy">
        <property name="idName" value="bizOrderId"/>
        <property name="appName" value="trade"/>
        <property name="databaseConfigs">
            <list>
                <bean class="com.oschina.sequence.DatabaseConfig">
                    <property name="host" value="10.10.23.46:3306"/>
                    <property name="userName" value="root"/>
                    <property name="password" value="123456"/>
                    <property name="databaseName" value="trade"/>
                </bean>
                <bean class="com.oschina.sequence.DatabaseConfig">
                    <property name="host" value="10.10.23.47:3306"/>
                    <property name="userName" value="root"/>
                    <property name="password" value="123456"/>
                    <property name="databaseName" value="trade"/>
                </bean>
            </list>
        </property>
    </bean>
```

4.  在你的应用程序中，则可以如下使用，需要多个不同的ID你需要配置不同的bizOrderIdSequence：
```
    Long bizOrderId = bizOrderIdSequence.nextValue();
```